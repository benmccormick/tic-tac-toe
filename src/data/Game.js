import Board from "./Board";
import { X, O } from "./constants";
import HumanPlayer from "./HumanPlayer";

export default class Game {
  constructor(board, players, current) {
    this.players = players || [
      new HumanPlayer(X, "Player 1"),
      new HumanPlayer(O, "Player 2")
    ];
    this.board = board || new Board();
    this.current = current || 0;
  }

  get currentPlayer() {
    return this.players[this.current];
  }

  get status() {
    return this.board.status;
  }

  switchPlayer() {
    // the rare meaningful use of a bitwise operator in JS
    this.current = this.current ^ 1;
  }

  move(x, y) {
    try {
      let newBoard = this.board.markSquare(x, y, this.currentPlayer.type);
      this.board = newBoard;
    } catch (err) {
      throw err;
    }
    this.switchPlayer();
    return new Game(this.board, this.players, this.current);
  }
}
