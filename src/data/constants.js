// cell values
export const X = "x",
  O = "o",
  EMPTY = "empty";

// game states
export const X_WIN = "x_win",
  O_WIN = "o_win",
  TIE = "tie",
  IN_PROGRESS = "in_progress";

// provide some short hands for the 9 cells
export const TL = [0, 0],
  T = [1, 0],
  TR = [2, 0],
  L = [0, 1],
  M = [1, 1],
  R = [2, 1],
  BL = [0, 2],
  B = [1, 2],
  BR = [2, 2];
