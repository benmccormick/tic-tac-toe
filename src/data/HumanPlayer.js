export default class HumanPlayer {
  constructor(type, name) {
    this.type = type;
    this.name = name;
  }
  // this is for bots, no implementation here
  automove() {
    return undefined;
  }
}
