import {
  EMPTY,
  X,
  O,
  X_WIN,
  O_WIN,
  TIE,
  IN_PROGRESS,
  TL,
  T,
  TR,
  L,
  M,
  R,
  BL,
  B,
  BR
} from "./constants";

const getInitial = () => [
  [EMPTY, EMPTY, EMPTY],
  [EMPTY, EMPTY, EMPTY],
  [EMPTY, EMPTY, EMPTY]
];

// there are only 8 winning sets, and diagonals are annoying
// so easier to just enumerate them than go through
// loops to generate the logic

const winningSets = [
  [TL, T, TR],
  [L, M, R],
  [BL, B, BR],
  [TL, L, BL],
  [T, M, B],
  [TR, R, BR],
  [TL, M, BR],
  [BL, M, TR]
];

export default class Board {
  constructor(state) {
    this.board = state || getInitial();
  }

  getCells() {
    return this.board;
  }

  // returns a status object with {status: X_WIN|O_WIN|TIE|IN_PROGRESS, winningCells:[]}
  get status() {
    // right now this was optimized for easy of writing code quickly,
    // not speed or beauty.  We're just running through the possible winning sets twice
    // and that's ok, because its only 8 * 2 * 3 cells getting touched each time (small n)
    for (let wSet of winningSets) {
      if (wSet.every(([x, y]) => this.board[y][x] === X)) {
        return {
          status: X_WIN,
          winningCells: wSet
        };
      }
      if (wSet.every(([x, y]) => this.board[y][x] === O)) {
        return {
          status: O_WIN,
          winningCells: wSet
        };
      }
    }
    if (this.board.every(row => row.every(cell => cell !== EMPTY))) {
      return {
        status: TIE,
        winningCells: []
      };
    }
    return {
      status: IN_PROGRESS,
      winningCells: []
    };
  }

  markSquare(x, y, val) {
    let { board } = this;
    if (x < 0 || x >= board[0].length || y < 0 || y >= board.length) {
      throw new Error("Square Is Off The Board");
    }
    if (board[y][x] !== EMPTY) {
      throw new Error("Square Is Already Occupied");
    }
    if (val !== X && val !== O) {
      throw new Error("Unknown value for square");
    }
    // this is inefficient and potentially buggy,
    // would clean up if this was a real app.
    // Ideally we wouldn't be mutating the current object before cloning
    board[y][x] = val;
    return new Board(board);
  }
}
