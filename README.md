# git-gat-go

This is a quick learning project for me to play around with Vue, Vue CLI, and GitLab.

If you want to clone it, you can spin up the local server with

```
yarn # install deps
yarn serve # Open local dev server
```

and then go to localhost:8080 to play a Git flavored game of Tic Tac Toe.
