// https://docs.cypress.io/api/introduction/api.html

describe("Sanity Check", () => {
  it("Visits the app root url", () => {
    cy.visit("/");
    cy.contains("h1", "Git-Gat-Go");
  });

  it("Can click a square and fill it in", () => {
    let firstCell = '[data-cell="0 0"]';
    let firstCellDiv = '[data-cell="0 0"] > div';
    cy.visit("/");
    cy.get(firstCellDiv)
      .children()
      .should("have.length", 0);
    cy.get(firstCell).click();
    cy.get(firstCellDiv)
      .children()
      .should("have.length", 1);
  });
});
