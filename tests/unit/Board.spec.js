import Board from "@/data/Board";
import { EMPTY, O, X } from "@/data/constants";
import {
  IN_PROGRESS,
  TIE,
  X_WIN,
  L,
  M,
  R,
  O_WIN,
  TL,
  TR,
  T,
  B,
  BR
} from "../../src/data/constants";

describe("The Board class", () => {
  it("Fills with empty cells by default", () => {
    let board = new Board();
    expect(board.getCells()).toEqual([
      [EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY],
      [EMPTY, EMPTY, EMPTY]
    ]);
  });

  it("Also takes an existing position as a starting point", () => {
    let startingPoint = [
      [EMPTY, EMPTY, O],
      [EMPTY, X, EMPTY],
      [EMPTY, EMPTY, EMPTY]
    ];
    let board = new Board(startingPoint);
    expect(board.getCells()).toEqual(startingPoint);
  });

  it("When marking a square, it throws an error on illegal move", () => {
    let startingPoint = [
      [EMPTY, EMPTY, O],
      [EMPTY, X, EMPTY],
      [EMPTY, EMPTY, EMPTY]
    ];
    let board = new Board(startingPoint);
    expect(() => {
      board.markSquare(-1, 0, X);
    }).toThrowError("Square Is Off The Board");

    expect(() => {
      board.markSquare(2, 0, X);
    }).toThrowError("Square Is Already Occupied");

    expect(() => {
      board.markSquare(1, 0, "Bad Value");
    }).toThrowError("Unknown value for square");
  });

  it("When marking a square, it returns a new board when successful", () => {
    let startingPoint = [
      [EMPTY, EMPTY, O],
      [EMPTY, X, EMPTY],
      [EMPTY, EMPTY, EMPTY]
    ];
    let expectedEnd = [[X, EMPTY, O], [EMPTY, X, EMPTY], [EMPTY, EMPTY, EMPTY]];
    let board = new Board(startingPoint);
    let newBoard = board.markSquare(0, 0, X);
    expect(newBoard.getCells()).toEqual(expectedEnd);
  });

  it("The board keeps status in sync and can handle different game situations", () => {
    let inProgressExample = [
      [EMPTY, EMPTY, O],
      [EMPTY, X, EMPTY],
      [EMPTY, EMPTY, EMPTY]
    ];
    expect(new Board(inProgressExample).status).toEqual({
      status: IN_PROGRESS,
      winningCells: []
    });
    let tieExample = [[O, X, O], [O, X, X], [X, O, X]];
    expect(new Board(tieExample).status).toEqual({
      status: TIE,
      winningCells: []
    });
    let xRowWinExample = [[EMPTY, O, O], [X, X, X], [EMPTY, EMPTY, EMPTY]];

    expect(new Board(xRowWinExample).status).toEqual({
      status: X_WIN,
      winningCells: [L, M, R]
    });
    let oRowWinExample = [[O, O, O], [EMPTY, X, X], [EMPTY, EMPTY, EMPTY]];

    expect(new Board(oRowWinExample).status).toEqual({
      status: O_WIN,
      winningCells: [TL, T, TR]
    });

    let xColumnWinExample = [[O, O, X], [EMPTY, X, X], [EMPTY, O, X]];

    expect(new Board(xColumnWinExample).status).toEqual({
      status: X_WIN,
      winningCells: [TR, R, BR]
    });
    let oColumnWinExample = [[O, O, X], [X, O, X], [EMPTY, O, EMPTY]];
    expect(new Board(oColumnWinExample).status).toEqual({
      status: O_WIN,
      winningCells: [T, M, B]
    });
    let oDiagonalWinExample = [[O, O, X], [X, O, X], [EMPTY, EMPTY, O]];
    expect(new Board(oDiagonalWinExample).status).toEqual({
      status: O_WIN,
      winningCells: [TL, M, BR]
    });
    let xDiagonalWinExample = [[X, O, O], [EMPTY, X, X], [EMPTY, O, X]];
    expect(new Board(xDiagonalWinExample).status).toEqual({
      status: X_WIN,
      winningCells: [TL, M, BR]
    });
  });
});
